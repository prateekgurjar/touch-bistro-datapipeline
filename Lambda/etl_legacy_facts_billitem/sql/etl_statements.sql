Begin Transaction;

DELETE FROM legacy_facts_billitem
USING {} staging
WHERE legacy_facts_billitem.billitem_uuid = staging.billitem_uuid
AND legacy_facts_billitem.bill_number = staging.bill_number
AND staging.load_timestamp_utc > legacy_facts_billitem.load_timestamp_utc;

INSERT INTO legacy_facts_billitem
SELECT DISTINCT staging.* FROM {} staging
WHERE staging.load_timestamp_utc > (SELECT max(load_timestamp_utc) from legacy_facts_billitem) ;

END Transaction;


