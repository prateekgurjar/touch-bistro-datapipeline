Begin Transaction;

DELETE FROM legacy_facts_bill
USING {} AS staging
WHERE legacy_facts_bill.bill_uuid = staging.bill_uuid
AND staging.load_timestamp_utc > legacy_facts_bill.load_timestamp_utc;

INSERT INTO legacy_facts_bill
SELECT staging.* FROM {} staging
WHERE staging.load_timestamp_utc > (SELECT max(load_timestamp_utc) FROM legacy_facts_bill)
;

END Transaction;


