import os
import lib.helpers as helpers

def lambda_handler(event, context):
    
    time = str(event['time'])
    source_bucket = helpers.set_source_bucket()
    destination_table = helpers.get_destination_table()
    table_to_drop = helpers.get_dropping_table(destination_table, time)
    
    filepath = helpers.get_file_path(destination_table, time)
    print(f'processing {filepath}')
    key = filepath.split(f'{source_bucket}/')[1]
    staging_table = helpers.get_staging_table(destination_table, time)

    drop_statement = (helpers.generate_sql('sql/drop_table.sql')).format(table_to_drop)
    copy_error_handling_statement = (helpers.generate_sql('sql/copy_error_handling_statement.sql')).format(destination_table)
    create_statement = (helpers.generate_sql('sql/create_statement.sql')).format(staging_table)

    helpers.execute_sql(drop_statement)
    helpers.execute_sql(create_statement)
    
    if helpers.s3_check_object(source_bucket, key) == 1:
        
        copy_statement = (helpers.generate_sql('sql/copy_statement.sql')).format(staging_table, filepath, os.environ['iam_arn'])
        etl_statement = (helpers.generate_sql('sql/etl_statements.sql')).format(staging_table, staging_table)
        
        helpers.execute_copy(copy_statement, copy_error_handling_statement, filepath)
        helpers.execute_sql(etl_statement)
    
    else:
        print("Invalid object or Empty Bucket")