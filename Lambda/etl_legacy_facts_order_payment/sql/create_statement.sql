BEGIN TRANSACTION;

CREATE SCHEMA IF NOT EXISTS staging;

CREATE TABLE IF NOT EXISTS {} (
business_date date NULL SORTKEY,
venue_xref_id int NULL DISTKEY,
bill_number int NULL,
payment_method_name nvarchar (255) NULL,
payment_total_applied decimal (14,2) NULL,
closed_payment_uuid varchar NULL,
payment_paid_at_local timestamp NULL, 
payment_paid_at_utc timestamp NULL, 
bill_uuid nvarchar (49) NULL,
order_uuid nvarchar (49) NULL,
order_number int NULL, 
payment_total_paid decimal (16,2) NULL,
payment_tip_applied decimal (14,2) NULL,
payment_change_tendered decimal (14,2) NULL,
payment_tipout_to_house decimal (20,8) NULL,
legacy_base_restaurant_id int NULL, 
legacy_bill_id int NULL, 
load_timestamp_utc timestamp NULL,
load_batch_uuid nvarchar (49) NULL
);

END TRANSACTION;