Begin Transaction;

DELETE FROM legacy_facts_order_payment
USING {} AS staging
WHERE legacy_facts_order_payment.closed_payment_uuid = staging.closed_payment_uuid
AND staging.load_timestamp_utc > legacy_facts_order_payment.load_timestamp_utc;

INSERT INTO legacy_facts_order_payment
SELECT staging.* FROM {} staging
WHERE staging.load_timestamp_utc > (SELECT max(load_timestamp_utc) FROM legacy_facts_order_payment)
;

END Transaction;