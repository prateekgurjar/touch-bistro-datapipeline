import os
import lib.helpers as helpers

def lambda_handler(event, context):

    time = str(event['time'])
    
    helpers.notification(time)

    source_bucket = helpers.set_source_bucket()
    failed_objects_bucket = helpers.set_failed_object_bucket()

    failed_table_list = ['seed_legacy_facts_bill', 'seed_legacy_facts_billitem', 'seed_legacy_facts_discountitem', 'seed_legacy_facts__order_payment', 'seed_legacy_facts_staff_shift']
    
    create_statement = (helpers.generate_sql('sql/create_statement.sql'))
    helpers.execute_sql(create_statement)

    for table in failed_table_list:

        filepath_list = helpers.get_failed_files(table)

        for filepath in filepath_list:

            dest_staging_table = f'staging.failed_{filepath.split("/")[0]}'
            copy_statement = (helpers.generate_sql('sql/copy_statement.sql')).format(dest_staging_table, filepath, os.environ['iam_arn'])
            #print(copy_statement)
            helpers.execute_copy_failed_objects(copy_statement, filepath)

    etl_statement = (helpers.generate_sql('sql/etl_statements.sql'))
    helpers.execute_sql(etl_statement)