SELECT trim(filename) AS input FROM 
stl_load_errors sl JOIN stv_tbl_perm sp
ON sl.tbl = sp.id
AND trim(name) like '%{}%'
ORDER BY starttime DESC
LIMIT 1;