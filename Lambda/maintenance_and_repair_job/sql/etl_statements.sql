Begin Transaction;

DELETE FROM legacy_facts_bill
USING staging.failed_seed_legacy_facts_bill AS staging
WHERE legacy_facts_bill.bill_uuid = staging.bill_uuid
AND staging.load_timestamp_utc > legacy_facts_bill.load_timestamp_utc;

INSERT INTO legacy_facts_bill
SELECT DISTINCT staging.* FROM staging.failed_seed_legacy_facts_bill staging
WHERE staging.load_timestamp_utc > (SELECT max(load_timestamp_utc) FROM legacy_facts_bill)
;

END Transaction;

Begin Transaction;

DELETE FROM legacy_facts_billitem
USING staging.failed_seed_legacy_facts_billitem staging
WHERE legacy_facts_billitem.billitem_uuid = staging.billitem_uuid
AND legacy_facts_billitem.bill_number = staging.bill_number
AND staging.load_timestamp_utc > legacy_facts_billitem.load_timestamp_utc;

INSERT INTO legacy_facts_billitem
SELECT DISTINCT staging.* FROM staging.failed_seed_legacy_facts_billitem staging
WHERE staging.load_timestamp_utc > (SELECT max(load_timestamp_utc) from legacy_facts_billitem) ;

END Transaction;


Begin Transaction;

DELETE FROM legacy_facts_discountitem
USING staging.failed_seed_legacy_facts_discountitem AS staging
WHERE legacy_facts_discountitem.discountitem_uuid = staging.discountitem_uuid
AND staging.load_timestamp_utc > legacy_facts_discountitem.load_timestamp_utc;

INSERT INTO legacy_facts_discountitem
SELECT DISTINCT staging.* FROM staging.failed_seed_legacy_facts_discountitem staging
WHERE staging.load_timestamp_utc > (SELECT max(load_timestamp_utc) FROM legacy_facts_discountitem)
;

END Transaction;

Begin Transaction;

DELETE FROM legacy_facts_order_payment
USING staging.failed_seed_legacy_facts_order_payment AS staging
WHERE legacy_facts_order_payment.closed_payment_uuid = staging.closed_payment_uuid
AND staging.load_timestamp_utc > legacy_facts_order_payment.load_timestamp_utc;

INSERT INTO legacy_facts_order_payment
SELECT DISTINCT staging.* FROM staging.failed_seed_legacy_facts_order_payment staging
WHERE staging.load_timestamp_utc > (SELECT max(load_timestamp_utc) FROM legacy_facts_order_payment)
;

END Transaction;

Begin Transaction;

DELETE FROM legacy_facts_staff_shift
USING staging.failed_seed_legacy_facts_staff_shift AS staging
WHERE legacy_facts_staff_shift.shift_uuid = staging.shift_uuid
AND staging.load_timestamp_utc > legacy_facts_staff_shift.load_timestamp_utc;

INSERT INTO legacy_facts_staff_shift
SELECT DISTINCT staging.* FROM staging.failed_seed_legacy_facts_staff_shift staging
WHERE staging.load_timestamp_utc > (SELECT max(load_timestamp_utc) FROM legacy_facts_staff_shift)
;

END Transaction;