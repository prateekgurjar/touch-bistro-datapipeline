import json, psycopg2, boto3, datetime, os, sys, base64, threading
from botocore.exceptions import ClientError

s3 = boto3.resource('s3', aws_access_key_id='AKIAYW57NVD4M7MEP5NB', aws_secret_access_key= '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')

def set_source_bucket():
    source_bucket = os.environ['source_bucket']
    return source_bucket

def set_failed_object_bucket():
    failed_objects_bucket = os.environ['failed_objects_bucket']
    return failed_objects_bucket

def notification(time):
    
    time_ts = datetime.datetime.strptime(time, "%Y-%m-%dT%H:%M:%SZ")
    prev_4_hours = time_ts - datetime.timedelta(hours=4)
    hour = int(datetime.datetime.strftime(prev_4_hours, "%H"))
    minute = int(datetime.datetime.strftime(prev_4_hours, "%M"))
    
    failed_table_list = ['seed_legacy_facts_bill', 'seed_legacy_facts_billitem', 'seed_legacy_facts_discountitem', 'seed_legacy_facts__order_payment', 'seed_legacy_facts_staff_shift']
    
    if hour==0 and minute==0:
        s3 = boto3.resource('s3')
        bucket = s3.Bucket(set_failed_object_bucket())
        failed_obj = []
        
        for table in failed_table_list:
            obj = list(bucket.objects.filter(Prefix=table))
            failed_obj.extend(obj)

        if len(failed_obj) > 0:
            sns = boto3.client('sns')
            response = sns.publish(TopicArn=os.environ['notification_arn'], Message=f'Objects found in {set_failed_object_bucket()} bucket. Please fix the corrupted files')
    else:
        pass
 
def get_destination_table():
    destination_table = os.environ['destination_table']
    return destination_table

def get_file_path(table_name, time):

    time_ts = datetime.datetime.strptime(time, "%Y-%m-%dT%H:%M:%SZ")
    previous_5_min = time_ts - datetime.timedelta(seconds=300)
    previous_5_min = previous_5_min - datetime.timedelta(hours=4)

    minute = int(datetime.datetime.strftime(previous_5_min, "%M"))
    if (minute//5)*5 == 0:
        min_folder = '00'
    elif (minute//5)*5 == 5:
        min_folder = '05'
    else:
        min_folder = str((minute//5)*5)

    logical_partition = datetime.datetime.strftime(previous_5_min, "%Y/%m/%d/%H")
    file_path = f's3://{set_source_bucket()}/{table_name}/{logical_partition}/{min_folder}'
    return file_path
    
def get_staging_table(table_name, time):
    
    time_ts = datetime.datetime.strptime(time, "%Y-%m-%dT%H:%M:%SZ")
    previous_5_min = time_ts - datetime.timedelta(seconds=300)
    previous_4_hour = previous_5_min - datetime.timedelta(hours=4)
    logical_partition = datetime.datetime.strftime(previous_4_hour, "%Y/%m/%d")
    staging_table = 'staging.'+f'{table_name}/{logical_partition}'.replace("/", "_")
    return staging_table

def get_dropping_table(table_name, time):

    retention_days = int(os.environ['retention_period'])
    time_ts = datetime.datetime.strptime(time, "%Y-%m-%dT%H:%M:%SZ")
    previous_5_min = time_ts - datetime.timedelta(seconds=300)
    previous_4_hour = previous_5_min - datetime.timedelta(hours=4)
    retention_ts = previous_4_hour - datetime.timedelta(days=retention_days)
    logical_partition = datetime.datetime.strftime(retention_ts, "%Y/%m/%d")
    dropping_table = 'staging.'+f'{table_name}/{logical_partition}'.replace("/", "_")
    return dropping_table

def create_connection():
    
    secrets = json.loads(get_secret())
    host = secrets['host']
    dbname = os.environ['db_name']
    user = secrets['username']
    password = secrets['password']
    port = secrets['port']
    conn_string = "host={} dbname={} user={} password={} port={}".format(host, dbname, user, password, port)
    conn = psycopg2.connect(conn_string)
    return conn

def s3_copy_objects(source_bucket, failed_objects_bucket, failed_file_name):
    
    s3 = boto3.resource('s3')
    key = failed_file_name
    copy_source = {'Bucket': source_bucket,'Key': key}
    print(f'Starting process to copy {failed_file_name} from {source_bucket} to {failed_objects_bucket}')
    
    try:
        s3.meta.client.copy(copy_source, failed_objects_bucket, key)
        print(f'{key} copied to {failed_objects_bucket} bucket')
    except Exception as e:
        s3.meta.client.copy(copy_source, failed_objects_bucket, key)
        print(e)
    else:
        print('Failed to move objects')

def s3_delete_failed_object(bucket_name, failed_file_name):
    s3 = boto3.resource('s3')
    s3.Object(bucket_name, failed_file_name).delete()

def s3_check_object(bucket_name, key):
    
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)
    obj = list(bucket.objects.filter(Prefix=key))
    print(f'Objects in folder {len(obj)}')
    if len(obj) > 0:
        return 1
    else:
        return 0

def generate_sql(file_name):
    
    f=open(file_name, "r")
    if f.mode == 'r':
        contents =f.read()
    return contents

def execute_sql(sql_statement):
    
    conn = create_connection()
    cur = conn.cursor()
    
    try:
        cur.execute(sql_statement)
        conn.commit()
        
    except Exception as e:
        print(e)
        
    conn.close()

def execute_copy_failed_objects(sql_statement, failed_file_name):
    
    conn = create_connection()
    cur = conn.cursor()
    failed_objects_bucket = set_source_bucket()
    source_bucket = set_failed_object_bucket()
    try:
        cur.execute(sql_statement)
        conn.commit()
        bucket = s3.Bucket(source_bucket)
        filenames = [obj.key for obj in bucket.objects.filter(Prefix=failed_file_name)]
        for fname in filenames:
            t = threading.Thread(target = copy_job, args=(fname,)).start()
            s3_delete_failed_object(source_bucket, fname)
    except Exception as e:
        print(e)
        
    conn.close()

def get_failed_files(key):
    
    s3 = boto3.resource('s3', aws_access_key_id='AKIAYW57NVD4M7MEP5NB', aws_secret_access_key= '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')
    failed_objects_bucket = set_failed_object_bucket()
    bucket = s3.Bucket(failed_objects_bucket)
    copy_source = {'Bucket': failed_objects_bucket,'Key': key}
    
    file_list = []
    for index, obj in enumerate(bucket.objects.filter(Prefix=key)):
    
        get_path = obj.key.split('/')
        del get_path[-2:]
        seperator = '/'
        file = seperator.join(get_path)
        file_list.append(file)
        if index == 100000:
            break
        
    return list(set(file_list))

def get_secret():

    secret_name = os.environ['secret_key']
    region_name = "ca-central-1"

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name
    )

    try:
        get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
        else:
            decoded_binary_secret = base64.b64decode(get_secret_value_response['SecretBinary'])
            
    return get_secret_value_response['SecretString']

def copy_job(fname):
    copy_source = {'Bucket': set_failed_object_bucket(),'Key': fname}
    bucket = s3.Bucket(set_failed_object_bucket())
    obj = bucket.Object(fname)
    try:
        #print(f'copying {file}')
        s3.meta.client.copy_object(CopySource=copy_source, Bucket=set_source_bucket(), Key=fname)
    except Exception as e:
        print(fname)
        print(e)

def execute_copy(copy_statement, failure_handle_statement, file_path):
    
    source_bucket = set_source_bucket()
    failed_objects_bucket = set_failed_object_bucket()
    conn = create_connection()
    cur = conn.cursor()
    allowed_attempts = int(os.environ['retry_attempts'])
    success_flag = 0
    retry_attempt = 0
    
    try:
        cur.execute(copy_statement)
        conn.commit()
        success_flag = 1
    except Exception as e:
        print(e)
        while retry_attempt < allowed_attempts:
            print(f'Trying attempt {retry_attempt}')
            retry_attempt+= 1
            conn.rollback()
            cur.execute(failure_handle_statement)
            failed_file_obj = cur.fetchone()
            file_name = failed_file_obj[0].split(f's3://{source_bucket}/')[1].strip()
            conn.commit()
            s3_copy_failed_objects(source_bucket, failed_objects_bucket, file_name)
            s3_delete_failed_object(source_bucket, file_name)
            try:
                cur.execute(copy_statement)
                conn.commit()
                success_flag = 1
                break
            except Exception as e:
                print(e)
                conn.rollback()
        
    if success_flag == 1:
        if retry_attempt == 0:
            print('Copy objects successful')
        else:
            print(f'Copy successful after {retry_attempt} attempts')
            print(f'Error files placed in {failed_objects_bucket}. Please fix and rerun the Failed objects lambda function')
    else:
        print(f'More than 3 csv files corrupted.')
        bucket = s3.Bucket(source_bucket)
        key = file_path
        copy_source = {'Bucket': source_bucket,'Key': key}
        filenames = [obj.key for obj in bucket.objects.filter(Prefix=key)]
        key = file_path.split(f's3://{source_bucket}/')[1].strip()
        for fname in filenames:
            t = threading.Thread(target = copy_job, args=(fname,)).start()
        #s3_delete_failed_object(source_bucket, key)

    conn.close()