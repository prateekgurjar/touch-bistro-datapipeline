Begin Transaction;

DELETE FROM legacy_facts_discountitem
USING {} AS staging
WHERE legacy_facts_discountitem.discountitem_uuid = staging.discountitem_uuid
AND staging.load_timestamp_utc > legacy_facts_discountitem.load_timestamp_utc;

INSERT INTO legacy_facts_discountitem
SELECT DISTINCT staging.* FROM {} staging
WHERE staging.load_timestamp_utc > (SELECT max(load_timestamp_utc) FROM legacy_facts_discountitem)
;

END Transaction;


