Begin Transaction;

CREATE SCHEMA IF NOT EXISTS staging;

CREATE TABLE IF NOT EXISTS {}(
business_date date NULL SORTKEY,
venue_xref_id int NULL DISTKEY,
shift_type_name nvarchar (4000) NULL,
shift_uuid nvarchar (49) NULL,
shift_type_uuid nvarchar (49) NULL,
shift_waiter_uuid nvarchar (255) NULL,
shift_waiter_name nvarchar (255) NULL,
shift_started_at_local timestamp NULL,
shift_finished_at_local timestamp NULL,
shift_started_at_utc timestamp NULL,
shift_finished_at_utc timestamp NULL,
shift_rate_hourly decimal (14, 2),
shift_duration_in_hours decimal (17,6),
shift_duration_in_minutes decimal (17,6),
legacy_shift_type_is_back_of_house bool NULL,
legacy_base_restaurant_id int NULL,
legacy_base_waiter_id int NULL,
legacy_shift_id int NULL,
legacy_shift_type_id int NULL,
load_timestamp_utc timestamp NULL,
load_batch_uuid nvarchar (49) NULL
);

End Transaction;