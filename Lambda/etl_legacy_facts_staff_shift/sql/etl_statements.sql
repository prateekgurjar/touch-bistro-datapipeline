Begin Transaction;

DELETE FROM legacy_facts_staff_shift
USING {} AS staging
WHERE legacy_facts_staff_shift.shift_uuid = staging.shift_uuid
AND staging.load_timestamp_utc > legacy_facts_staff_shift.load_timestamp_utc;

INSERT INTO legacy_facts_staff_shift
SELECT staging.* FROM {} staging
WHERE staging.load_timestamp_utc > (SELECT max(load_timestamp_utc) FROM legacy_facts_staff_shift)
;

END Transaction;


